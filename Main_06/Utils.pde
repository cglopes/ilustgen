/////////////////////////////////////// Inicio do código - import de bibliotecas, guardar nomes e fazer associações //<>//
String[] loadFicheiroTexto() {
  String stringTexto = RiTa.loadString("rapunzel_edit.txt", this);
  return RiTa.getPosTagsInline(stringTexto).split(" ");
}

Table loadFicheiroTabelaSentimentos() {
  return loadTable("sentimentos.csv", "header");
}

Texto getConstrucaoTexto(String[] palavras, Table sentimentos) {

  Texto textoObj = new Texto();
  textoObj.nomesProprios = new ArrayList<Nome>();
  textoObj.nomesComuns = new ArrayList<Nome>();
  textoObj.adjetivos = new ArrayList<Adjetivo>();
  textoObj.verbos = new ArrayList<Verbo>();

  Nome nomeContexto = null;
  int paragrafo = 0;
  for (int i = 0; i< palavras.length; i++) {
    Adjetivo adjetivo=null;
    Verbo verbo=null;
    String[] palavraTag = split(palavras[i], '/');
    String palavra = palavraTag[0];
    //String palavra = palavraTag[0].split("'")[0];
    String tag = palavraTag[1];

    print(palavra + " ");

    if (palavra.equals("#")) {
      paragrafo++;
    }

    //Nomes proprios
    if (tag.equals("nnp") || tag.equals("nnps")) {
      textoObj.nomesProprios.add(nomeContexto=getNomeProprio(palavras, palavra, i, paragrafo));
    }

    //Nomes comuns
    if (tag.equals("nn")) {
      textoObj.nomesComuns.add(nomeContexto=getNomeComum(palavras, palavra, i, paragrafo));
    }

    // Verbos
    if (tag.equals("vb") || tag.equals("vbd") || tag.equals("vbg") || tag.equals("vbn") || tag.equals("vbp") || tag.equals("vbz")) {
      textoObj.verbos.add( verbo = getVerbo(palavra, i, paragrafo, sentimentos));
    }

    // Adjetivos
    if (tag.equals("jj") || tag.equals("jjr") || tag.equals("jjs")) {
      textoObj.adjetivos.add( adjetivo = getAdjetivo(palavra, i, paragrafo, sentimentos));
    }

    analizaContexto(nomeContexto, adjetivo, verbo);
    //delay(1000);
  }

  return textoObj;
}

void analizaContexto(Nome nome, Adjetivo adjetivo, Verbo verbo) {
  if (nome!=null) {       
    if (adjetivo != null) {
      nome.adjetivos.add(adjetivo);
      print("(adjetivo de "+nome.nome+") ");
    }
    if (verbo != null) {
      nome.verbos.add(verbo);
      print("(verbo de "+nome.nome+") ");
    }
  }
}

Palavra getPalavra(String nome, int id, int paragrafo) {
  Palavra p = new Palavra();
  p.id = id;
  p.nome = nome;
  p.nrParagrafo = paragrafo; // TODO
  return p;
}

Nome getNome(String nome, int id, int paragrafo) {
  Palavra p = getPalavra(nome, id, paragrafo);
  Nome n = new Nome(p);
  return n;
}

Nome getNomeProprio(String[] palavras, String nome, int id, int paragrafo) {
  Nome n = getNome(nome, id, paragrafo);
  n.frequencia = getFrequencia(palavras, n.nome);
  n.raio = map(n.frequencia, 1, 25, 15, 100);
  return n;
}

Nome getNomeComum(String[] palavras, String nome, int id, int paragrafo) {
  Nome n = getNome(nome, id, paragrafo);
  n.frequencia = getFrequencia(palavras, n.nome);
  n.raio = map(n.frequencia, 1, 40, 5, 100);
  return n;
}

Adjetivo getAdjetivo(String nome, int id, int paragrafo, Table tabelaSentimentos) {
  Palavra p = getPalavra(nome, id, paragrafo);
  Adjetivo ad = new Adjetivo(p);
  ad.sentimentos = getSentimentos(nome, tabelaSentimentos);
  return ad;
}

Verbo getVerbo(String nome, int id, int paragrafo, Table tabelaSentimentos) {
  Palavra p = getPalavra(nome, id, paragrafo);
  Verbo v = new Verbo(p);
  v.sentimentos = getSentimentos(v.nome, tabelaSentimentos);
  return v;
}

///////////////////////////////////////////////////////ORDENA POR FREQUENCIA/RETIRA "'S"
int getFrequencia(String[] texto, String palavra) {

  int freq = 0;
  for (int i = 0; i < texto.length; i++) {
    String[] split = split(texto[i], '/');
    String palavraTexto = split[0];

    if (palavra.charAt(palavra.length()-1) == 's' && palavra.charAt(palavra.length()-2) == '’') {
      palavra = palavra.substring(0, palavra.length() - 1);
      palavra = palavra.substring(0, palavra.length() - 1);
    }
    if (palavraTexto.charAt(palavraTexto.length()-1) == 's' && palavraTexto.charAt(palavraTexto.length()-2) == '’') {
      palavraTexto = palavraTexto.substring(0, palavraTexto.length() - 1);
      palavraTexto = palavraTexto.substring(0, palavraTexto.length() - 1);
    }

    if (palavraTexto.equals(palavra)) {
      freq++;
    }
  }   
  println("freq="+freq);
  return freq;
}


// Ordena uma lista de palavras por frequência (Maior -> Menor)
ArrayList<Nome> ordenarFrequenciaProprios(ArrayList<Nome> lista) {
  ArrayList<Nome> listaOrdenada = new ArrayList<Nome>();
  int index = 0;
  Nome max;
  while (lista.size()!=listaOrdenada.size()) { 
    max = lista.get(index++);
    max.frequencia = -1;
    for (Nome p : lista) {
      if (p.frequencia >= max.frequencia && !listaOrdenada.contains(p)) {
        max = p;
      }
    }
    Nome temp = max;
    listaOrdenada.add(temp);
  }
  return listaOrdenada;
}

///////////////////////////////////////////SENTIMENTOS
ArrayList<String> getSentimentos(String palavra, Table tabelaSentimentos) {

  ArrayList<String> sentimentos = new ArrayList<String>();

  for (TableRow row : tabelaSentimentos.rows()) {

    String[] linhaSplit = split(row.getString(0), "--");
    String[] linhaSplit2 = split(linhaSplit[1].trim(), "/");

    boolean check = false;

    if (linhaSplit[0].trim().equals(palavra)) check = true;

    for (String s : linhaSplit2) {
      if (s.trim().equals(palavra)) check = true;
    }

    if (!sentimentos.contains(row.getString(1)) && check == true) {
      sentimentos.add(row.getString(1));
    }
  }

  return sentimentos;
}


ArrayList <String> getListaPosNeg (ArrayList<Adjetivo> getAdjetivosGerais) { //ArrayList que retira todos os sentimentos do texto

  ArrayList<String> listaSentimentos = new ArrayList<String>();

  for (int i = 0; i < getAdjetivosGerais.size(); i++) {
    if (listaSentimentos.get(i).equals("disgust") 
      || listaSentimentos.get(i).equals("trust") 
      || listaSentimentos.get(i).equals("anticip") 
      || listaSentimentos.get(i).equals("joy")
      || listaSentimentos.get(i).equals("fear") 
      || listaSentimentos.get(i).equals("surprise") 
      || listaSentimentos.get(i).equals("sadness") 
      || listaSentimentos.get(i).equals("anger")) {
      listaSentimentos.remove(i);
    }
  }

  return listaSentimentos;
}


ArrayList<String> getSentimentosByNome(Nome nome) {
  ArrayList<String> sentimentos = new ArrayList<String>();
  for (Adjetivo adjetivo : nome.adjetivos) {
    for (String sentimento : adjetivo.sentimentos) {
      sentimentos.add(sentimento);
    }
  }
  for (Verbo verbo : nome.verbos) {
    for (String sentimento : verbo.sentimentos) {
      sentimentos.add(sentimento);
    }
  }
  return sentimentos;
}


Float verificaIntensidadeSentimentosGeral (Nome nome, String sentimento) {
  ArrayList<String> sentimentosNome = getSentimentosByNome(nome);
  int contaSentimento = 0, totalSentimentos = sentimentosNome.size();
  for (String s : sentimentosNome) {
    if (s.toLowerCase().equals(sentimento.toLowerCase())) {
      contaSentimento++;
    }
  }
  float intensidade = 0;
  if (totalSentimentos != 0) {
    intensidade = map(contaSentimento, 0, totalSentimentos, 0, 1);
  }
  return intensidade;
}

Integer contaPositivos (ArrayList<String> sentimentos) {
 int positivos = 0;
  for (String s : sentimentos ) {
    if (s.toLowerCase().equals("joy") || s.toLowerCase().equals("anger")) {
      positivos++;
    }
  }
  return positivos;
}


Integer contaNegativos (ArrayList<String> sentimentos) {
  int negativos = 0;
  for (String s : sentimentos) {
    if (s.toLowerCase().equals("fear") || s.toLowerCase().equals("sadness")) {
      negativos++;
    }
  }
  return negativos;
}

Integer saturacaoPos (int positivos, ArrayList<String> sentimentos) {
  int saturacao = int(map(positivos, 0, sentimentos.size(), 80, 100));
  return saturacao;
}

Integer saturacaoNeg (int negativos, ArrayList<String> sentimentos){
  int saturacao = int(map(negativos, 0, sentimentos.size(), 70, 40));
  return saturacao;
}



HashMap setCoresPalavras(ArrayList<Palavra> palavras) {
  color[] cores = getCores();
  HashMap<String, Integer> corNomeProprio = new HashMap<String, Integer>(); 
  int corNova = 0;
  for (Palavra np : palavras) {
    Object value = corNomeProprio.get(np.nome);
    if (value != null) {//palavra duplicada
      color cor = cores[corNomeProprio.get(np.nome)];
      np.cor = cor;
    } else { //palavra nova
      color cor = cores[corNova];
      corNomeProprio.put(np.nome, corNova);
      np.cor = cor;
      corNova++;
    }
  }
  return corNomeProprio;
}


HashMap setCoresPalavrasPositivas(ArrayList<Palavra> palavras, ArrayList<String> sentimentos) {
  int m = saturacaoPos(contaPositivos(sentimentos), sentimentos);
  color[] cores = getCoresPositivas(m);
  HashMap<String, Integer> corNomeProprio = new HashMap<String, Integer>(); 
  int corNova = 0;

  for (Palavra np : palavras) {
    Object value = corNomeProprio.get(np.nome);
    if (value != null) {//palavra duplicad
      color cor = cores[corNomeProprio.get(np.nome)];
      np.cor = cor;
    } else { //palavra nova
      color cor = cores[corNova];
      corNomeProprio.put(np.nome, corNova);
      np.cor = cor;
      corNova++;
    }
  }
  return corNomeProprio;
}


HashMap setCoresPalavrasNegativas(ArrayList<Palavra> palavras, ArrayList<String> sentimentos) {
  int m = saturacaoNeg(contaNegativos(sentimentos), sentimentos);
  color[] cores = getCoresNegativas(m);
  HashMap<String, Integer> corNomeProprio = new HashMap<String, Integer>(); 
  int corNova = 0;


  for (Palavra np : palavras) {
    Object value = corNomeProprio.get(np.nome);
    if (value != null) {//palavra duplicada
      color cor = cores[corNomeProprio.get(np.nome)];
      np.cor = cor;
    } else { //palavra nova
      color cor = cores[corNova];
      corNomeProprio.put(np.nome, corNova);
      np.cor = cor;
      corNova++;
    }
  }
  return corNomeProprio;
}

void setCores (ArrayList<Palavra> palavras, ArrayList<String> sentimentos) {
  if (contaPositivos(sentimentos) > contaNegativos(sentimentos)) {
    setCoresPalavrasPositivas(palavras, sentimentos);
  } else if (contaPositivos(sentimentos) < contaNegativos(sentimentos)) {
    setCoresPalavrasNegativas(palavras, sentimentos);
  } else {
    setCoresPalavras(palavras);
  }

  println("positivos:" + contaPositivos(sentimentos));
}

///////////////////////////////////////////////////////DEFINE POSIÇÕES
void setPosicaoPalavras (ArrayList<Palavra> palavras) {
  int index = 0;
  for (Palavra p : palavras) {
    float m = map(index++, 0, palavras.size(), 0, width*height); 
    p.x =  m % width;
    p.y = m / width;
    //println("width:" + width + " height:" + height + " x:" + p.x + " y:" + p.y + " m:" + m + " size:" + palavrasDesenhar.size());
  }
}

////////////////////////////////////////////////COLISÕES

void setColisoesComCanvas (ArrayList<Palavra> palavras) {

  for (Palavra palavra : palavras) {
    if (palavra.x > width - palavra.raio) {
      palavra.x = width - palavra.raio;
      translate(palavra.x, palavra.y);
    } else if (palavra.x < palavra.raio) {
      palavra.x = palavra.raio;
      translate(palavra.x, palavra.y);
    } else if (palavra.y > height - palavra.raio) {
      palavra.y = height-palavra.raio;
      translate (palavra.x, palavra.y);
    } else if (palavra.y < palavra.raio) {
      palavra.y = palavra.raio;
      translate (palavra.x, palavra.y);
    } else {
      translate(palavra.x, palavra.y);
    }
    //verificaSobreposicoes(x, y, raio);
  }
}

void verificaSobreposicoes (float x, float y, float raio, ArrayList<Palavra> getPalavrasComPosicao) {
  for (int i = 0; i < getPalavrasComPosicao.size(); i++) {
    if (dist(getPalavrasComPosicao.get(0).x, getPalavrasComPosicao.get(0).y, getPalavrasComPosicao.get(i).x, getPalavrasComPosicao.get(i).y) > raio) {
      translate(x + raio, y + raio);
    }
  }
}

///////////////////////////////////////DIVIDE PARÁGRAFOS
ArrayList<String> divideParagrafos(String texto[]) {

  ArrayList<String> paragrafos = new ArrayList<String>();
  String par = "";

  for (int i = 0; i < texto.length; i++) {
    if (texto.equals('#')) {
      //par = texto.subString(0, i);
      paragrafos.add(par);
    } 
    for (int x = 0; x < paragrafos.size(); x++) {
      if (paragrafos.get(x).equals("\n")) paragrafos.remove(x);
    }
  }
  return paragrafos;
}

void keyPressed (ArrayList<String> paragrafos) {
  
  if (keyCode == RIGHT) {
    for (int i = 0; i > paragrafos.size(); i++) {
      paragrafos.get(i);
    }
  } else if (keyCode == LEFT) {
    for (int i = paragrafos.size(); i > 0; i--) {
      paragrafos.get(i);
    }
  } else {
    draw();
  }
}