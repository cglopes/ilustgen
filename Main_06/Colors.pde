///////////////////////////////////////////////////////CORES

color[] getCoresNegativas(int m) {

  color[] cores = 
    {
    color(217, m, 68), 
    color(27, m, 94), 
    color(49, m, 100), 
    color(76, m, 78), 
    color(176, m, 81), 
    color(196, m, 93), 
    color(295, m, 57), 
    color(321, m, 100), 
    color(0, m, 75), 

    color(105, m, 72), 
    color(187, m, 47), 
    color(1, m, 88), 
    color(64, m, 54), 
    color(327, m, 74), 
    color(302, m, 78), 
    color(32, m, 58), 
    color(240, m, 56), 

    color(317, 32, 43), 
    color(334, 73, 69), 
    color(240, 24, 80), 
    color(0, 100, 38), 
    color(49, 73, 34), 
    color(0, 20, 65), 
    color(12, 100, 89), 
    color(340, 100, 81), 
    color(255, 63, 100), 
 
  };
  return cores;
}


color[] getCoresPositivas(int n) {

  color[] cores = 
    {
    color(200, n, 57), 
    color(27, n, 94), 
    color(49, n, 90), 
    color(76, n, 57), 
    color(172, n, 55), 
    color(190, n, 78), 
    color(290, n, 50), 
    color(321, n, 84), 
    color(359, n, 69), 

    color(101, n, 41), 
    color(182, n, 30), 
    color(341, n, 65), 
    color(73, n, 28), 
    color(321, n, 56), 
    color(297, n, 56), 
    color(31, n, 56), 
    color(241, n, 27), 

    color(317, 32, 43), 
    color(334, 73, 69), 
    color(240, 24, 80), 
    color(0, 100, 38), 
    color(49, 73, 34), 
    color(0, 20, 65), 
    color(12, 100, 89), 
    color(340, 100, 81), 
    color(255, 63, 100), 

  };
  return cores;
}

color[] getCores() {

  color[] cores = 
    {
    color(208, 70, 69), 
    color(26, 70, 94), 
    color(49, 74, 100), 
    color(76, 74, 69), 
    color(176, 74, 72), 
    color(196, 74, 93), 
    color(295, 74, 57), 
    color(321, 74, 100), 
    color(359, 70, 80), 

    color(104, 75, 67), 
    color(187, 74, 41), 
    color(0, 74, 88), 
    color(66, 74, 52), 
    color(327, 74, 74), 
    color(302, 74, 72), 
    color(31, 74, 61), 
    color(240, 74, 48), 

    color(317, 32, 43), 
    color(334, 73, 69), 
    color(240, 24, 80), 
    color(0, 100, 38), 
    color(49, 73, 34), 
    color(0, 20, 65), 
    color(12, 100, 89), 
    color(340, 100, 81), 
    color(255, 63, 100), 

  };
  return cores;
}