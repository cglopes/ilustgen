
It was the middle of winter, and the snowflakes were falling from the sky like feathers. Now, a Queen sat sewing at a window framed in black ebony, and as she sewed she looked out upon the snow. Suddenly she pricked her finger and three drops of blood fell on to the snow. And the red looked so lovely on the white that she thought to herself: ‘If only I had a child as white as snow and as red as blood, and as black as the wood of the window frame!’ Soon after, she had a daughter, whose hair was black as ebony, while her cheeks were red as blood, and her skin as white as snow; so she was called Snowdrop. But when the child was born the Queen died. A year after the King took another wife. She was a handsome woman, but proud and overbearing, and could not endure that any one should surpass her in beauty. She had a magic looking-glass, and when she stood before it and looked at herself she used to say:

‘Mirror, Mirror on the wall,
Who is fairest of us all?’
then the Glass answered,

‘Queen, thou’rt fairest of them all.’
Then she was content, for she knew that the Looking-glass spoke the truth.

But Snowdrop grew up and became more and more beautiful, so that when she was seven years old she was as beautiful as the day, and far surpassed the Queen. Once, when she asked her Glass,

‘Mirror, Mirror on the wall,
Who is fairest of us all?’
it answered—

‘Queen, thou art fairest here, I hold,
But Snowdrop is fairer a thousandfold.’
Then the Queen was horror-struck, and turned green and yellow with jealousy. From the hour that she saw Snowdrop her heart sank, and she hated the little girl.

The pride and envy of her heart grew like a weed, so that she had no rest day nor night. At last she called a Huntsman, and said: ‘Take the child out into the wood; I will not set eyes on her again; you must kill her and bring me her lungs and liver as tokens.’

The Huntsman obeyed, and took Snowdrop out into the forest, but when he drew his hunting-knife and was preparing to plunge it into her innocent heart, she began to cry:

‘Alas! dear Huntsman, spare my life, and I will run away into the wild forest and never come back again.’

And because of her beauty the Huntsman had pity on her and said, ‘Well, run away, poor child.’ Wild beasts will soon devour you, he thought, but still he felt as though a weight were lifted from his heart because he had not been obliged to kill her. And as just at that moment a young fawn came leaping by, he pierced it and took the lungs and liver as tokens to the Queen. The Cook was ordered to serve them up in pickle, and the wicked Queen ate them thinking that they were Snowdrop’s.

Now the poor child was alone in the great wood, with no living soul near, and she was so frightened that she knew not what to do. Then she began to run, and ran over the sharp stones and through the brambles, while the animals passed her by without harming her. She ran as far as her feet could carry her till it was nearly evening, when she saw a little house and went in to rest. Inside, everything was small, but as neat and clean as could be. A small table covered with a white cloth stood ready with seven small plates, and by every plate was a spoon, knife, fork, and cup. Seven little beds were ranged against the walls, covered with snow-white coverlets. As Snowdrop was very hungry and thirsty she ate a little bread and vegetable from each plate, and drank a little wine from each cup, for she did not want to eat up the whole of one portion. Then, being very tired, she lay down in one of the beds. She tried them all but none suited her; one was too short, another too long, all except the seventh, which was just right. She remained in it, said her prayers, and fell asleep.

When it was quite dark the masters of the house came in. They were seven Dwarfs, who used to dig in the mountains for ore. They kindled their lights, and as soon as they could see they noticed that some one had been there, for everything was not in the order in which they had left it.

The first said, ‘Who has been sitting in my chair?’

The second said, ‘Who has been eating off my plate?’

The third said, ‘Who has been nibbling my bread?’

The fourth said, ‘Who has been eating my vegetables?’

The fifth said, ‘Who has been using my fork?’

The sixth said, ‘Who has been cutting with my knife?’

The seventh said, ‘Who has been drinking out of my cup?’

Then the first looked and saw a slight impression on his bed, and said, ‘Who has been treading on my bed?’ The others came running up and said, ‘And mine, and mine.’ But the seventh, when he looked into his bed, saw Snowdrop, who lay there asleep. He called the others, who came up and cried out with astonishment, as they held their lights and gazed at Snowdrop. ‘Heavens! what a beautiful child,’ they said, and they were so delighted that they did not wake her up but left her asleep in bed. And the seventh Dwarf slept with his comrades, an hour with each all through the night.

When morning came Snowdrop woke up, and when she saw the seven Dwarfs she was frightened.

But they were very kind and asked her name.

‘I am called Snowdrop,’ she answered.

‘How did you get into our house?’ they asked.

Then she told them how her stepmother had wished to get rid of her, how the Huntsman had spared her life, and how she had run all day till she had found the house.

Then the Dwarfs said, ‘Will you look after our household, cook, make the beds, wash, sew and knit, and keep everything neat and clean? If so you shall stay with us and want for nothing.’

‘Yes,’ said Snowdrop, ‘with all my heart’; and she stayed with them and kept the house in order.

In the morning they went to the mountain and searched for copper and gold, and in the evening they came back and then their meal had to be ready. All day the maiden was alone, and the good Dwarfs warned her and said, ‘Beware of your stepmother, who will soon learn that you are here. don’t let any one in.’

But the Queen, having, as she imagined, eaten Snowdrop’s liver and lungs, and feeling certain that she was the fairest of all, stepped in front of her Glass, and asked—

‘Mirror, Mirror on the wall,
Who is fairest of us all?’
the Glass answered as usual—

‘Queen, thou art fairest here, I hold,
But Snowdrop over the fells,
Who with the seven Dwarfs dwells,
Is fairer still a thousandfold.’
She was dismayed, for she knew that the Glass told no lies, and she saw that the Hunter had deceived her and that Snowdrop still lived. Accordingly she began to wonder afresh how she might compass her death; for as long as she was not the fairest in the land her jealous heart left her no rest. At last she thought of a plan. She dyed her face and dressed up like an old Pedlar, so that she was quite unrecognisable. In this guise she crossed over the seven mountains to the home of the seven Dwarfs and called out, ‘Wares for sale.’

Snowdrop peeped out of the window and said, ‘Good-day, mother, what have you got to sell?’

‘Good wares, fine wares,’ she answered, ‘laces of every colour’; and she held out one which was made of gay plaited silk.

‘I may let the honest woman in,’ thought Snowdrop, and she unbolted the door and bought the pretty lace.

‘Child,’ said the Old Woman, ‘what a sight you are, I will lace you properly for once.’

Snowdrop made no objection, and placed herself before the Old Woman to let her lace her with the new lace. But the Old Woman laced so quickly and tightly that she took away Snowdrop’s breath and she fell down as though dead.

‘Now I am the fairest,’ she said to herself, and hurried away.

Not long after the seven Dwarfs came home, and were horror-struck when they saw their dear little Snowdrop lying on the floor without stirring, like one dead. When they saw she was laced too tight they cut the lace, whereupon she began to breathe and soon came back to life again. When the Dwarfs heard what had happened, they said that the old Pedlar was no other than the wicked Queen. ‘Take care not to let any one in when we are not here,’ they said.

Now the wicked Queen, as soon as she got home, went to the Glass and asked—

‘Mirror, Mirror on the wall,
Who is fairest of us all?’
and it answered as usual—

‘Queen, thou art fairest here, I hold,
But Snowdrop over the fells,
Who with the seven Dwarfs dwells,
Is fairer still a thousandfold.’
When she heard it all her blood flew to her heart, so enraged was she, for she knew that Snowdrop had come back to life again. Then she thought to herself, ‘I must plan something which will put an end to her.’ By means of witchcraft, in which she was skilled, she made a poisoned comb. Next she disguised herself and took the form of a different Old Woman. She crossed the mountains and came to the home of the seven Dwarfs, and knocked at the door calling out, ‘Good wares to sell.’

Snowdrop looked out of the window and said, ‘Go away, I must not let any one in.’

‘At least you may look,’ answered the Old Woman, and she took the poisoned comb and held it up.

The child was so pleased with it that she let herself be beguiled, and opened the door.

When she had made a bargain the Old Woman said, ‘Now I will comb your hair properly for once.’

Poor Snowdrop, suspecting no evil, let the Old Woman have her way, but scarcely was the poisoned comb fixed in her hair than the poison took effect, and the maiden fell down unconscious.

‘You paragon of beauty,’ said the wicked woman, ‘now it is all over with you,’ and she went away.

Happily it was near the time when the seven Dwarfs came home. When they saw Snowdrop lying on the ground as though dead, they immediately suspected her stepmother, and searched till they found the poisoned comb. No sooner had they removed it than Snowdrop came to herself again and related what had happened. They warned her again to be on her guard, and to open the door to no one.

When she got home the Queen stood before her Glass and said—

‘Mirror, Mirror on the wall,
Who is fairest of us all?’
and it answered as usual—

‘Queen, thou art fairest here, I hold,
But Snowdrop over the fells,
Who with the seven Dwarfs dwells,
Is fairer still a thousandfold.’
When she heard the Glass speak these words she trembled and quivered with rage. ‘Snowdrop shall die,’ she said, ‘even if it cost me my own life.’ Thereupon she went into a secret room, which no one ever entered but herself, and made a poisonous apple. Outwardly it was beautiful to look upon, with rosy cheeks, and every one who saw it longed for it, but whoever ate of it was certain to die. When the apple was ready she dyed her face and dressed herself like an old Peasant Woman and so crossed the seven hills to the Dwarfs’ home. There she knocked.

Snowdrop put her head out of the window and said, ‘I must not let any one in, the seven Dwarfs have forbidden me.’

‘It is all the same to me,’ said the Peasant Woman. ‘I shall soon get rid of my apples. There, I will give you one.’

‘No; I must not take anything.’

‘Are you afraid of poison?’ said the woman. ‘See, I will cut the apple in half: you eat the red side and I will keep the other.’

Now the apple was so cunningly painted that the red half alone was poisoned. Snowdrop longed for the apple, and when she saw the Peasant Woman eating she could hold out no longer, stretched out her hand and took the poisoned half. Scarcely had she put a bit into her mouth than she fell dead to the ground.

The Queen looked with a fiendish glance, and laughed aloud and said, ‘White as snow, red as blood, and black as ebony, this time the Dwarfs cannot wake you up again.’ And when she got home and asked the Looking-glass—

‘Mirror, Mirror on the wall,
Who is fairest of us all?’
it answered at last—

‘Queen, thou’rt fairest of them all.’
Then her jealous heart was at rest, as much at rest as a jealous heart can be. The Dwarfs, when they came at evening, found Snowdrop lying on the ground and not a breath escaped her lips, and she was quite dead. They lifted her up and looked to see whether any poison was to be found, unlaced her dress, combed her hair, washed her with wine and water, but it was no use; their dear child was dead. They laid her on a bier, and all seven sat down and bewailed her and lamented over her for three whole days. Then they prepared to bury her, but she looked so fresh and living, and still had such beautiful rosy cheeks, that they said, ‘We cannot bury her in the dark earth.’ And so they had a transparent glass coffin made, so that she could be seen from every side, laid her inside and wrote on it in letters of gold her name and how she was a King’s daughter. Then they set the coffin out on the mountain, and one of them always stayed by and watched it. And the birds came too and mourned for Snowdrop, first an owl, then a raven, and lastly a dove.

Now Snowdrop lay a long, long time in her coffin, looking as though she were asleep. It happened that a Prince was wandering in the wood, and came to the home of the seven Dwarfs to pass the night. He saw the coffin on the mountain and lovely Snowdrop inside, and read what was written in golden letters. Then he said to the Dwarfs, ‘Let me have the coffin; I will give you whatever you like for it.’

But they said, ‘We will not give it up for all the gold of the world.’

Then he said, ‘Then give it to me as a gift, for I cannot live without Snowdrop to gaze upon; and I will honour and reverence it as my dearest treasure.’

When he had said these words the good Dwarfs pitied him and gave him the coffin.

The Prince bade his servants carry it on their shoulders. Now it happened that they stumbled over some brushwood, and the shock dislodged the piece of apple from Snowdrop’s throat. In a short time she opened her eyes, lifted the lid of the coffin, sat up and came back to life again completely.

‘O Heaven! where am I?’ she asked.

The Prince, full of joy, said, ‘You are with me,’ and he related what had happened, and then said, ‘I love you better than all the world; come with me to my father’s castle and be my wife.’

Snowdrop agreed and went with him, and their wedding was celebrated with great magnificence. Snowdrop’s wicked stepmother was invited to the feast; and when she had put on her fine clothes she stepped to her Glass and asked—

‘Mirror, Mirror on the wall,
Who is fairest of us all?’
The Glass answered—

‘Queen, thou art fairest here, I hold,
The young Queen fairer a thousandfold.’
Then the wicked woman uttered a curse, and was so terribly frightened that she didn’t know what to do. Yet she had no rest: she felt obliged to go and see the young Queen. And when she came in she recognised Snowdrop, and stood stock still with fear and terror. But iron slippers were heated over the fire, and were soon brought in with tongs and put before her. And she had to step into the red-hot shoes and dance till she fell down dead.