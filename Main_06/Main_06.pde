
import http.requests.*;
import rita.*;
import java.util.Map;

void setup() {

  size(1280, 700);
  background(255);
  colorMode(HSB, 360, 100, 100);

  String[] textoStr = loadFicheiroTexto();
  Table sentimentos = loadFicheiroTabelaSentimentos();
  Texto texto = getConstrucaoTexto(textoStr,sentimentos);
  texto.draw();
  
    for(Nome palavra : texto.nomesProprios){
      println("Adjetivos ["+palavra.nome+"]:"+ palavra.adjetivos.size());
    }
  
}