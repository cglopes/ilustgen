class Palavra {
  String nome;
  int id;
  float raio;
  color cor;
  int nrParagrafo;
  float x;
  float y;
  String forma;
  int intensidade;
  int saturacao;

  void draw() {
    fill(this.cor);
    pushMatrix();
    translate(this.x, this.y);
    noStroke();
    if (this.forma.equals("anger")) {
      defineFormaAnger(this.intensidade, this.raio);
    } else if (this.forma.equals("joy")) {
      defineFormaJoy(this.intensidade, this.raio);
    } else if (this.forma.equals("fear")) {
      defineFormaFear(this.intensidade, this.raio);
    } else if (this.forma == "sadness") {
      defineFormaSadness(this.intensidade, this.raio);
    } else if (this.forma == "") {
      defineFormaNeutra(this.raio);
    }
    popMatrix();
  }
}

class Nome extends Palavra {
  int frequencia;
  ArrayList<Adjetivo> adjetivos;
  ArrayList<Verbo> verbos;
  Nome(Palavra p) {
    this.adjetivos = new ArrayList<Adjetivo>();
    this.verbos = new ArrayList<Verbo>();
    this.nome = p.nome;
    this.id = p.id;
    this.raio = p.raio;
    this.cor = p.cor;
    this.saturacao = p.saturacao;
    this.nrParagrafo = p.nrParagrafo;
    this.x = p.x;
    this.y= p.y;
  }

  void draw() {

    String [] sentimentos = {"anger", "joy", "sadness", "fear"};

    float max = -1;
    float imax = -1;
    for (int i=0; i<4; i++) {
      if (verificaIntensidadeSentimentosGeral(this, sentimentos[i]) > max) {
        max = verificaIntensidadeSentimentosGeral(this, sentimentos[i]);
        imax = i;
      }
    }

    this.forma=sentimentos[int(imax)];
    this.intensidade =int(max);
    println(sentimentos[int(imax)]);
    println("HERE!");

    println("centro="+x+","+y);

    //pushMatrix();
    // translate(this.x, this.y);
    /* if (verificaIntensidadeSentimentosGeral(this, "anger") > verificaIntensidadeSentimentosGeral(this, "surprise")) {
     this.forma = "anger";
     this.intensidade = verificaIntensidadeSentimentosGeral(this, "anger");
     } else if (verificaIntensidadeSentimentosGeral(this, "anger") <= verificaIntensidadeSentimentosGeral(this, "surprise")) {
     this.forma = "surprise";
     this.intensidade = verificaIntensidadeSentimentosGeral(this, "surprise");
     } else if (verificaIntensidadeSentimentosGeral(this, "anger") > verificaIntensidadeSentimentosGeral(this, "anticip")) {
     this.forma = "anger";
     this.intensidade = verificaIntensidadeSentimentosGeral(this, "anger");
     } else if (verificaIntensidadeSentimentosGeral(this, "anger") < verificaIntensidadeSentimentosGeral(this, "anticip")) {
     this.forma = "anticip";
     this.intensidade = verificaIntensidadeSentimentosGeral(this, "anticip");
     } else if (verificaIntensidadeSentimentosGeral(this, "anger") > verificaIntensidadeSentimentosGeral(this, "fear")) {
     this.forma = "anger";
     this.intensidade = verificaIntensidadeSentimentosGeral(this, "anger");
     } else if (verificaIntensidadeSentimentosGeral(this, "anger") < verificaIntensidadeSentimentosGeral(this, "fear")) {
     this.forma = "fear";
     this.intensidade = verificaIntensidadeSentimentosGeral(this, "fear");
     } else if (verificaIntensidadeSentimentosGeral(this, "anticip") > verificaIntensidadeSentimentosGeral(this, "fear")) {
     this.forma = "anticip";
     this.intensidade = verificaIntensidadeSentimentosGeral(this, "anticip");
     } else if (verificaIntensidadeSentimentosGeral(this, "anticip") < verificaIntensidadeSentimentosGeral(this, "fear")) {
     this.forma = "fear";
     this.intensidade = verificaIntensidadeSentimentosGeral(this, "fear");
     } else if (verificaIntensidadeSentimentosGeral(this, "anticip") > verificaIntensidadeSentimentosGeral(this, "surprise")) {
     this.forma = "anticip";
     this.intensidade = verificaIntensidadeSentimentosGeral(this, "anticip");
     } else if (verificaIntensidadeSentimentosGeral(this, "anticip") < verificaIntensidadeSentimentosGeral(this, "surprise")) {
     this.forma = "susprise";
     this.intensidade = verificaIntensidadeSentimentosGeral(this, "surprise");
     } else if (verificaIntensidadeSentimentosGeral(this, "fear") > verificaIntensidadeSentimentosGeral(this, "surprise")) {
     this.forma = "fear";
     this.intensidade = verificaIntensidadeSentimentosGeral(this, "fear");
     } else if (verificaIntensidadeSentimentosGeral(this, "fear") < verificaIntensidadeSentimentosGeral(this, "surprise")) {
     this.forma = "surprise";
     this.intensidade = verificaIntensidadeSentimentosGeral(this, "surprise");
     } else if (verificaIntensidadeSentimentosGeral(this, "disgust") > 1) {
     this.forma = "disgust";
     this.intensidade = verificaIntensidadeSentimentosGeral(this, "disgust");
     } else if (verificaIntensidadeSentimentosGeral(this, "trust") > 1) {
     this.forma = "trust";
     this.intensidade = verificaIntensidadeSentimentosGeral(this, "trust");
     } else if (verificaIntensidadeSentimentosGeral(this, "joy") > 1) {
     this.forma = "joy";
     this.intensidade = verificaIntensidadeSentimentosGeral(this, "joy");
     } else if (verificaIntensidadeSentimentosGeral(this, "sadness") > 1) {
     this.forma = "sadness";
     this.intensidade = verificaIntensidadeSentimentosGeral(this, "sadness");
     } else {
     this.forma = "";
     this.intensidade = 0;
     }*/
    // fill(255,0,0);
    // ellipse(this.x, this.y, 200,200);
    super.draw();
    //popMatrix();
  }
}

class Verbo extends Palavra {

  ArrayList<String> sentimentos;
  Verbo(Palavra p) {
    this.nome = p.nome;
    this.id = p.id;
    this.raio = p.raio;
    this.cor = p.cor;
    this.nrParagrafo = p.nrParagrafo;
    this.x = p.x;
    this.y= p.y;
  }
}

class Adjetivo extends Palavra {
  ArrayList<String> sentimentos;
  Adjetivo(Palavra p) {
    this.nome = p.nome;
    this.id = p.id;
    this.raio = p.raio;
    this.cor = p.cor;
    this.nrParagrafo = p.nrParagrafo;
    this.x = p.x;
    this.y= p.y;
  }
}


class Texto {
  ArrayList<Nome> nomesProprios;
  ArrayList<Nome> nomesComuns;
  ArrayList<Adjetivo> adjetivos;
  ArrayList<Verbo> verbos;

  Texto() {
    nomesProprios = new ArrayList<Nome>();
    nomesComuns = new ArrayList<Nome>();
    adjetivos = new ArrayList<Adjetivo>();
    verbos = new ArrayList<Verbo>();
  }


  void draw() {
    for (Nome nome : nomesProprios) {
      ArrayList<String> sentimentos = getSentimentosByNome(nome);
      setCores((ArrayList)this.nomesProprios, sentimentos);
    }
    setPosicaoPalavras((ArrayList)this.nomesProprios);
    //setColisoesComCanvas((ArrayList)this.nomesProprios); //-> BUG! verifica o x e y de todas as bolas excepto o valor y da primeira
    drawListaPalavras((ArrayList)this.nomesProprios, true);
  }
}