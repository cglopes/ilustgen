
////////////////////////////////////////// Manda desenhar
void drawListaPalavras(ArrayList<Palavra> palavras, boolean permiteDuplicados) {
  ArrayList<String> isDrawn = new ArrayList<String>();
  for (Palavra n : palavras) {    
    if (!isDrawn.contains(n.nome)) {
      n.draw();
      if (!permiteDuplicados)
        isDrawn.add(n.nome);
    }
  }
}

void drawSentimentos(ArrayList<String> sentimentos, boolean desenhaSobreposto) {
}


void drawParagrafos() {
  
}


void defineFormaNeutra (float raio) {
  int resolution = 200;
  float x = 1, y = 1;
  float t = 0;

  beginShape();
  for (float a = 0; a <= TWO_PI; a+= TWO_PI/resolution) {

    float nInt = 28.5;
    float nAmp = 0.97;

    float nVal = map(noise( cos(a)*nInt+1, sin(a)*nInt+1, t), 0.0, 1.0, nAmp, 1.0); // map noise value to match the amplitude

    x = cos(a)*raio *nVal;
    y = sin(a)*raio *nVal;

    vertex(x, y);
  }
  endShape();
}


void defineFormaFear (int verificaIntensidadeSentimentosGeral, float raio) {
  int resolution = 200;
  float x = 1, y = 1;
  float t = 0;

  beginShape();
  for (float a = 0; a <= TWO_PI; a+= TWO_PI/resolution) {

    float nInt = 25;
    float nAmp = 9;
    if (verificaIntensidadeSentimentosGeral <= 0.1) {
      nAmp = nAmp*verificaIntensidadeSentimentosGeral;
    } else if (verificaIntensidadeSentimentosGeral <= 0.5) {
      nAmp = random (0.85, 0.89);
    } else {
      nAmp = random(0.78, 0.85);
    }

    float nVal = map(noise( cos(a)*nInt+1, sin(a)*nInt+1, t), 0.0, 1.0, nAmp, 1.0); // map noise value to match the amplitude

    x = cos(a)*raio *nVal;
    y = sin(a)*raio *nVal;

    vertex(x, y);
  }
  endShape();
}


void defineFormaAnger (int verificaIntensidadeSentimentosGeral, float raio) {
  int resolution = 200;
  float x = 1, y = 1;
  float t = 0;
  float nAmp = 0;

  float nInt = 30;

  beginShape();
  for (float a=0; a<=TWO_PI; a+=TWO_PI/resolution) {

    nAmp = nAmp*verificaIntensidadeSentimentosGeral;
    float nVal = map(noise( cos(a)*nInt+1, sin(a)*nInt+1, t ), 0.0, 1.0, nAmp, 1.0); // map noise value to match the amplitude
    //intra variação

    x = cos(a)*raio *nVal;
    y = sin(a)*raio *nVal;

    vertex(x, y);
  }
  endShape(CLOSE);
}


void defineFormaJoy (int verificaIntensidadeSentimentosGeral, float raio) {
  int resolution = 200;
  float x = 1, y = 1;
  float t = 0;
  float nAmp = 0, nInt = 1;

  if (verificaIntensidadeSentimentosGeral <= 0.1) {
    nInt = 0.8;
    nAmp = 0.23;
  } else if (verificaIntensidadeSentimentosGeral <= 0.5) {
    nAmp = random (0.3, 0.45);
  } else {
    nAmp = random(0.45, 0.5);
  }

  beginShape();
  for (float a=0; a<=TWO_PI; a+=TWO_PI/resolution) {

    float nVal = map(noise( cos(a)*nInt+1, sin(a)*nInt+1, t ), 0.0, 1.0, nAmp, 1.0); // map noise value to match the amplitude

    x = cos(a)*raio *nVal;
    y = sin(a)*raio *nVal;

    vertex(x, y);
  }
  endShape(CLOSE);
}


void defineFormaSadness (int verificaIntensidadeSentimentosGeral, float raio) {

  int resolution = 200;
  float x = 1, y = 1;
  float t = 0;
  float nAmp = 9, nInt = 25;

  if (verificaIntensidadeSentimentosGeral <= 0.1) {
    nAmp = nAmp*verificaIntensidadeSentimentosGeral;
  } else if (verificaIntensidadeSentimentosGeral <= 0.5) {
    nAmp = random (0.85, 0.89);
  } else {
    nAmp = random(0.78, 0.85);
  }
  //nAmp = map(mouseY, 0, height, 0.0, 1.0); // map mouseY to noise amplitude

  //nInt = random(0, 1);
  //nAmp = random(0.5, 1);
  beginShape();
  for (float a=0; a<=TWO_PI; a+=TWO_PI/resolution) {

    float nVal = map(noise( cos(a)*nInt+1, sin(a)*nInt+1, t ), 0.0, 1.0, nAmp, 1.0); // map noise value to match the amplitude

    x = cos(a)*raio *nVal;
    y = sin(a)*raio *nVal;

    vertex(x, y);
  }
  endShape(CLOSE);
}